# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils webapp depend.php depend.apache

MY_P="${PF//}"

DESCRIPTION="WiPLExME (based on Siremis) is a web interface for provisioning of users of WiPLEx SIP Server."
HOMEPAGE="http://siremis.asipto.com/"
SRC_URI="http://siremis.asipto.com/pub/downloads/siremis/${MY_P}.tgz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=virtual/httpd-php-5.0"

need_php_httpd
need_apache

S="${WORKDIR}/${MY_P}"

pkg_setup () {
	webapp_pkg_setup
	use mysql && require_php_with_use mysql zlib xml
}

src_install() {
	webapp_src_preinst # cp -R * ${D}/${MY_HTDOCSDIR}

	dodoc ChangeLog README  || die
	rm -rf ChangeLog README

	insinto "${MY_HTDOCSDIR}"
	doins -r .

	local files="siremis siremis/log siremis/session siremis/files"
	#siremis/templates/cpl

	for file in ${files}; do
		webapp_serverowned "${MY_HTDOCSDIR}"/${file}
	done

	webapp_configfile  "${MY_HTDOCSDIR}"/siremis/Config.xml
	webapp_configfile  "${MY_HTDOCSDIR}"/openbiz/metadata/Config.xml

	#webapp_configfile  "${APACHE_VHOSTS_CONFDIR}"/httpd-wme.conf
	insinto "${APACHE_VHOSTS_CONFDIR}"
	doins "${FILESDIR}/httpd-wme.conf" || die

	#webapp_postinst_txt en "${FILESDIR}"/postinstall-en.txt
	webapp_src_install
}

pkg_postinst() {
	ewarn "webapp-config will not be run automatically"
	ewarn "Try to run webapp-config this way:"
	ewarn "webapp-config -I -h <host> -d siremis ${PN} ${PVR}"
	# webapp_pkg_postinst
	ewarn
	#ewarn "Follow the instructions in POST-INSTALL."
}
