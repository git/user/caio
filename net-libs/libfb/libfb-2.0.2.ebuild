# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit libtool

IUSE=""

DESCRIPTION="A library for Red-fone Fonulator utility"
HOMEPAGE="http://www.red-fone.com/"

SRC_URI="http://support.red-fone.com/downloads/fonulator/${P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"

DEPEND="net-libs/libpcap
        net-libs/libnet"

#MAKEOPTS="${MAKEOPTS} -j1"

src_compile() {
	elibtoolize
	econf
	emake || die "emake failed"
}

src_install() {

	einstall || die "einstall failed"
}
