# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils multilib

MY_PN="chan_ss7"

DESCRIPTION="Asterisk SS7 channel library"
HOMEPAGE="http://www.netfors.com/chan_ss7_free"
SRC_URI="http://www.netfors.com/media/download/${MY_PN}-${PV}.tar.gz"

#IUSE="-mtp3d -mtp3cli"
IUSE=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"

RDEPEND="
	>=net-misc/dahdi-2.3.0
	>=net-misc/asterisk-1.6.2"

DEPEND="${RDEPEND}"

MAKEOPTS="${MAKEOPTS} -j1"

S="${WORKDIR}/${MY_PN}-${PV}"

#src_unpack() {
#	unpack ${A}
#
#	cd ${S}
#	# tweak makefile
#	epatch ${FILESDIR}/${MY_PN}-0.8.4-gentoo.diff
#}

src_compile() {

	emake chan_ss7.so || die "emake failed"
}

src_install() {
	#make INSTALL_PREFIX=${D} install || die
	#emake DESTDIR=${D} install || die "Install failed"
	#INSTALL_PREFIX=/usr/lib/asterisk/modules make install || die "Install failed"
	#install -m 755 chan_ss7.so /usr/lib/asterisk/modules

	einfo "Copying ${S}/chan_ss7.so to /usr/$(get_libdir)/asterisk/modules"
	insinto /usr/$(get_libdir)/asterisk/modules
	doins ${S}/chan_ss7.so || die "install failed"

	dodoc README NEWS COPYING INSTALL ASTERISK_VARIABLES

	#insinto /etc/asterisk
	#doins ${FILESDIR}/ss7.conf.*

	#if [[ -n "$(egetent group asterisk)" ]]; then
	#	chown -R root:asterisk ${D}/etc/asterisk
	#	chmod -R u=rwX,g=rX,o= ${D}/etc/asterisk
	#fi
}

pkg_postinst() {
	einfo "Useful resources:"
	echo
	einfo "  http://www.voip-info.org/wiki/index.php?page=Asterisk+ss7+channels"
}
