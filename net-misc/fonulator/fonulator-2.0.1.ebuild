# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils multilib

IUSE=""

MY_P="${P/_/-}"

DESCRIPTION="foneBRIDGE run-time config utility"
HOMEPAGE="http://www.red-fone.com/"
SRC_URI="http://support.red-fone.com/downloads/fonulator/${MY_P}.tar.gz"

S="${WORKDIR}/${MY_P}"

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="amd64 x86"

RDEPEND=""

DEPEND="${RDEPEND} dev-libs/argtable
	net-libs/libfb"

src_compile() {
	econf --with-shared-libfb  # use shared libfb instead of static version
	emake || die "emake failed"
}

src_install() {

	einstall || die "einstall failed"
}
